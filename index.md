![](logo_resized.png)

# Helitaku

#####  

##### Contact 

<sanlisan@helitaku.com>

<https://codeberg.org/helitaku>

 

##  

##  

## Our philosophy

### Expression

Art will always be at the heart of our philosophy.

We need art today, more than ever. Communication is challenged everyday by the state of the world.   

Art transcends the barriers of language and culture.

When words are not sufficient anymore, other forms of expression light up the darkness.

### Decentralization

Our main objective is to provide the tools to allow anyone to share their creative content. Everyone has the right to express themselves. No matter their situation, way of living, or country. Full stop. Art is for everyone.

This is why our core philosophy is based on the decentralization of our technologies. Anyone should be able to host their own content without hassle, on their home infrastructure, a friend's or an entity they put their trust into.

Today, artists don't have to bind themselves to a revenue driven, post-capitalist content provider, anymore. This is the future, and we want to help make it happen.

### Transparency

Last but not least, Our tools are and will stay open-source. For all the reasons above, we strongly believe that full transparency is what will make our technology grow organically.

Crossbreeding produces mutation.

Mutation produces the art of tomorrow.

##  

## Helimura

 In response to our philosophy, we are working on a project named Helimura.

[Head out to this page](./helimura.html) for more information on the project. 

Demo <https://demo2025.helitaku.com/>

Source code <https://codeberg.org/helitaku/helimura>

 

## About us

Strong of almost ten years of experience in the animation film and video game industry as developer, we decided to take advantage of this acquired skill set and help the growth of a community close to our heart: digital art.

Opensource developers and digital artists have more common interest than one might assume. Collaborating on meaningful content, sharing, helping out each other, making people happy. Despite all this, both worlds have difficulties communicating with each other. 

We believe, at Helitaku, that we can become the bridge needed to take the best of both world and spread the beauty of creativity, through the endless maze of cut edge technologies. 
