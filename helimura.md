[Back to Helitaku](index.html)  

# Helimura

Demo <https://demo2025.helitaku.com/>

Source code <https://codeberg.org/helitaku/helimura>

![](demo.png)  


Helimura is a framework for serving digital content in the real world, at predetermined locations. Those predetermined locations are called *geofences*. You may consider these geofences as virtual boundaries of many digital gardens.

The content is collaboratively hosted on the decentralized network *IPFS*.\
![ipfs.png](./ipfs.png)  
Anyone owning a smartphone can access the digital content served by Helimura, provided they access a URL and activate the geolocation of their device.

![Picture.png](./Picture.png)   
Helimura was created as a mean for artist to reach to the world in an free, easy, and meaningful way. 

It is a framework for Art, in all its forms in mind.

However, its vesatility may extend its use to many other scopes, as advertising, culture, or education, to cite a few.\
 

# What Helimura is

## Frictionless

Every new solution seems to need an app nowadays. It is a hassle to install an app and open it anytime we want to use it. \
 

It is also well known that most app are closed sourced. 

By using them, we commit to respect their obscure terms of services, that might eventually end up in a breach of trust. Sometimes, we need to create an account, giving yet another service personal informations.\
 
![tonga.png](tonga.png)

Helimura takes a fundamental opposite approach by being as frictionless as possible. All is needed is to access a URL from a web browser.\
 

Web browser are evolving well and fast. So well, that nowadays, even 3D videogames, VR and AR can be loaded from a website.\
Helimura takes full advantage of this online experience, by serving any digital content as a micro-website. The only limit is creativity.\
 

## For an enhanced experience

"*Why would I go out if I can access as many contents from the comfort of my home?*"\
 

The use of smartphone has become such a convenience, that it seems obsolete to limit digital content to a specific place.

However, in the age of infinite scrolling, we consume more content than ever, while being as bored.

 

Helimura enhances the experience of any digital content, by making a location part of it.

Why not enjoy a song about nature, while walking near the river that inspired the artist?

Why not watch a short movie starring a restaurant while smelling its cuisine in the real world?

Why not visit the field of an historic battle, while listening to an audio guide from a History museum?

A content provided with Helimura is a unique occasion to enhance a user experience, enjoying it with all their senses. This allows the content creator to show more than what meets the eyes.\
 

## For equality 

### Between creators

Helimura is made with universality in mind. Every creator is invited to use it.

From world renowned artist, to weekend hobbyist, everyone can freely create geofences around the world. 

 

### Between providers

Because it lives on IPFS, everyone can become a host of the content, and make it accessible to the internet. No need for a private hosting service, friends and family can become your provider!

 

### Between developers

Helimura is opensource. Anyone can fork it and start their own flavor of the project. Or better, improve the original framework for everyone to benefit from it!

 

### And between users 

Being exhausted workers going home after a long shift, bored weekenders or digital art amateurs, everyone has access to digital content from Helimura.

Because it is as frictionless as possible, Helimura reaches every class and age in the population.

With geofences in your neighborhood, Art comes to you and seamlessly immerses itself in your everyday life.\
 

## Playful
![map.png](map.png)
The multitude of apps dedicated to geocaching and treasure hunting are proof that we enjoy exploring and discovering. This is what makes us...us

From child's play to the infamous internet search rabbit holes, we crave mysteries and seeking. \
Helimura scratches this itching by transforming any area into an Easter egg hunt.\
 

Places familiar to you become a new source of discovery. You can explore a new landscape in a place you already visited for hundreds of times.\
 

## Tamperproof

Because any data uploaded to IPFS is self-certified and has a unique, immutable fingerprint, Helimura can safely reference a link to a digital content, knowing it could never be replaced with malicious data. A modified version of the data would inherently generate a new fingerprint, and thus, a new URL.\
 

This makes the creation of virtual galleries unhackable, easy, and safe. IPFS removes the need of a trusted certificate authority, and a strong encrypted data transfer setup.\
 

# What Helimura is not

## A walled garden

A walled garden is a closed environment that purposely controls and restricts access to online content. It entices the user to interact more inside the closed environment, while making navigation outside the ecosystem challenging, or impossible.\
 

A good example of a walled garden is the use of messenger apps, when a user from one app cannot send messages to someone using a different app.\
 

All the data uploaded on IPFS and accessible with Helimura is in clear text. Anyone knowing the URL of a content has full access to its source code. By its design, Helimura cannot and won't ever be a walled garden.\
 

## Profit driven

Helimura in its core is not profit-driven. Being open source, and based on 100% open-source technologies, it will never close its source code for selling.\
 

This being said, anyone is free to fork it, and make an [enshittifiable](https://boingboing.net/2024/02/13/enshittification-is-coming-for-everything.html) variation of the framework.\
 

## A one-stop solution

Helimura in itself is agnostic and bare bone. As it is versatile enough to be adapted to various use cases, it also requires elbow greasing for extended features.\
 

# Example use cases

They are plenty, but we decided to choose just a few

* A contemporary art museum hosts an exposition about a sculpture artist. During the period of the exposition, bystanders can see 3D models of the sculptures in AR, while they are walking in the park near the entrance of the museum.![plan1.png](plan1.png)
  
* To boost tourism in a city rich in History, each point of interest has a geofence that showcases clips from film archives, starring the spot.

 ![](plan2.png)
  
* A writer makes their narration more entertaining, by giving two or more options about what comes next in each chapter. Depending on what option the user takes, they are invited to go to a different location, where the narration relates to their choice. \
   ![plan3.png](plan3.png)

# What comes next? Maybe...

At some point, new features will be added to Helimura. 

* Because of the collaborative aspect of content hosting on IPFS, people benevolently hosting the content could be rewarded for their computing power. For example, anyone hosting the music of an artist for a prolonged period of time, could receive a Thank You video, or tickets for the next live event from the artist. 
* Time-limited streaming of a live event in a geofenced place. People would gather and see the live-streamed content on their smartphone.
* IPFS is a promising technology that hasn't shown everything it can do yet. Due to its versatility and reliance on web browsers, Helimura will naturally continue to gain new features and possibilities. We are looking forward to where it will take us in the future.
